#include <stdio.h>

int get_profit(int attendees, int ticket);
int get_attendees(int ticket);

int main()
{
	printf("Ticket price and profit relation\n---------------------------------\n");
	printf("Ticket(Rs)  Attendees  Profit(Rs)\n");
	for (int i = 0; i < 30; i += 5)
	{
		int attendees = get_attendees(i);
		int profit = get_profit(attendees, i);
		printf("  %*d          %*d        %*d\n", 3, i, 3, attendees, 4, profit);
	}

	return 0;
}

int get_profit(int attendees, int ticket)
{
	return attendees * (ticket - 3) - 500;
}

int get_attendees(int ticket)
{
	return 120 + ((15 - ticket) * 4);
}
